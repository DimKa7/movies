<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMoviesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movies', function (Blueprint $table) {
            $table->id();
            $table->integer('show_id');
            $table->string('type');
            $table->string('title');
            // in real life application I will create a table for directors and cast_members
            // or maybe a persons table where I will save directors and cast members
            // and have a one to many relationship
            $table->string('director')->nullable();
            $table->text('cast')->nullable();
            $table->string('country')->nullable();
            //I supose that date_added date is the same as laravel created_at so i will not add it
            // $table->date('date_added');
            $table->year('release_year');
            $table->string('rating');
            $table->string('duration');
            $table->string('listed_in');
            $table->longText('description');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('movies');
    }
}
