<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//I didn't use auth middleware because I didn't implement the authentication
//in case i will need it I will use the passport and auth middleware to protect the routes
Route::apiResource('movies', 'MovieController');
