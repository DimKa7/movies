<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Movie extends Model
{
    //protect this field against mass-assignment
    protected $guarded = ['id', 'created_at', 'updated_at'];

    /**
     * Get the created at.
     *
     * @param  string  $value
     * @return string
     */
    public function getCreatedAtAttribute($value)
    {
        // cast the created at to the right format
        return date('F j, Y', strtotime($value));
    }
}
