<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreMovie;
use App\Movie;
use Illuminate\Http\Request;

class MovieController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $movies = Movie::latest()
            ->whereLike(['title', 'director', 'cast'], $request->search);

        $paginator = $movies->paginate($request->itemsPerPage);

        // in case we request a page greater then the last page from our result
        // we need to request the last page instead
        if ($request->page > $paginator->lastPage()) {
            $movies = $movies->paginate($request->itemsPerPage, ['*'], 'page', $paginator->lastPage());
        } else {
            $movies = $paginator;
        }

        return $movies;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\StoreMovie  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreMovie $request)
    {
        Movie::create($request->all());

        return response()->json([
            'status' => 'success'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Movie  $movie
     * @return \Illuminate\Http\Response
     */
    public function show(Movie $movie)
    {
        return $movie;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Movie  $movie
     * @return \Illuminate\Http\Response
     */
    public function update(StoreMovie $request, Movie $movie)
    {
        $movie->update($request->all());

        return response()->json([
            'status' => 'success'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Movie  $movie
     * @return \Illuminate\Http\Response
     */
    public function destroy(Movie $movie)
    {
        $movie->delete();

        return response()->json([
            'status' => 'success'
        ]);
    }
}
