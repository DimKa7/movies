<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreMovie extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'show_id' => 'required|integer',
            'type' => 'required|string|max:255',
            'title' => 'required|string|max:255',
            'director' => 'nullable|string|max:255',
            'country' => 'nullable|string|max:255',
            'release_year' => 'required|integer',
            'rating' => 'required|string|max:255',
            'duration' => 'required|string|max:255',
            'listed_in' => 'required|string|max:255',
            'description' => 'required'
        ];
    }
}
